#!/bin/sh

#Create Directories
mkdir bin
mkdir .dev
mkdir tmp
mkdir documents
mkdir projects
mkdir src/GitHub
mkdir src/Gitlab
mkdir src/Bitbucket
mkdir .repos

#Installing Git, cURL, wget etc.
sudo apt update
sudo apt install -y git curl wget automake

#Install Micro Text Editor
curl https://getmic.ro | bash
mv ~/micro ~/bin/micro

#Install ZSH
sudo apt install -y zsh
#Go ahead and start the new shell
zsh
#Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

#Install FISH
sudo apt update
sudo apt install -y fish
#and to add oh-my-fish:
curl -L https://get.oh-my.fish | fish
exit

#Install Nodejs:
cd .dev
#Debian and Ubuntu based Linux distributions
#Also including: Linux Mint, Linux Mint Debian Edition (LMDE), elementaryOS, bash on Windows and others.
#Node.js is available from the NodeSource Debian and Ubuntu binary distributions repository (formerly Chris Lea's Launchpad PPA). Support for this repository, along with its scripts, can be found on GitHub at nodesource/distributions.
#
#Uncomment For Node 8:
#curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
#sudo apt-get install -y nodejs
#
#Node.js 9:
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs
#
#Optional: install build tools (but highly recommended)
#To compile and install native addons from npm you may also need to install build tools:
sudo apt-get install -y build-essential
#
#While in the .dev dir lets go ahead and run npm or yarn so we have a package.json file.  It will make global installs much easier:
#sudo npm init #uncomment to fill in the script ourselves or we can run:
sudo npm install

#Installing yarn:(windows)
#curl --header 'Host: github-production-release-asset-2e65be.s3.amazonaws.com' --user-agent 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0' --header 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' --header 'Accept-Language: en-US,en;q=0.5' --referer 'https://yarnpkg.com/en/docs/install' --header 'Upgrade-Insecure-Requests: 1' 'https://github-production-release-asset-2e65be.s3.amazonaws.com/49970642/db6ce8a0-c061-11e7-9237-9d8b4fa520be?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20171213%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20171213T055032Z&X-Amz-Expires=300&X-Amz-Signature=01ea4b6a8f7da7ffa2fd8be6bafe0e328801a8175bed976470cf96d1813d2ce9&X-Amz-SignedHeaders=host&actor_id=20589181&response-content-disposition=attachment%3B%20filename%3Dyarn-1.3.2.msi&response-content-type=application%2Foctet-stream' --output 'yarn-1.3.2.msi'
#
#On Debian or Ubuntu Linux, you can install Yarn via our Debian package repository. You will first need to configure the repository:
#should still be in the .dev dir.
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
#Path Setup
#If you chose manual installation, the following steps will add Yarn to path variable and run it from anywhere.
#Note: your profile may be in your .profile, .bash_profile, .bashrc, .zshrc, etc.
#
#Add this to your profile:
export PATH="$PATH:/opt/yarn-1.3.2/bin" #(the path may vary depending on where you extracted Yarn to)
#In the terminal, log in and log out for the changes to take effect
#
#To have access to Yarn’s executables globally, you will need to set up the PATH environment variable in your terminal.
#To do this, add:
export PATH="$PATH:`yarn global bin`"
#to your profile.

#Install trash program for deleting files or directories
sudo apt install trash-cli
#And to empty
sudo npm i -g empty-trash-cli

#Back to home dir:
cd ~/

#I will also add an alias file to source to keep things consistent between shells as well.  My file typically will look like this:
##!/bin/sh
#List of personal used/created aliases:
#alias cls='clear'
#alias inst='sudo apt install'
#alias update='sudo apt update'
#alias upgrade='sudo apt upgrade'
#alias fup='sudo apt full-upgrade'
#alias ar='sudo apt-get autoremove'
#alias fb='sudo apt install --fix-broken'
#alias np='/usr/local/nodejs/bin/npm'
#alias ni='sudo /usr/local/nodejs/bin/npm install'
#alias ng='sudo /usr/local/nodejs/bin/npm  install -g'
#alias yi='sudo yarn install'
#alias yg='sudo yarn global add'
#alias ya='sudo yarn add'
#alias sce='source ~/.zshrc'
#alias micro='~/bin/micro'
#alias hyperconf='micro /mnt/c/Users/samue/.hyper.js'
#alias trsh='sudo trash'
#alias et='sudo empty-trash'
#
#To source it just do:
source ~/alias  #you can run this every shell you use or just add it to the .**rc file of the shell you are using
#
#Typcially I use the zsh so the file I add it to is the ~/.zshrc file.
#To add is to the ~/.zshrc file
echo "source ~/alias" >> ~/.zshrc

#Python install
cd .dev
curl --header 'Host: www.python.org' --user-agent 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0' --header 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' --header 'Accept-Language: en-US,en;q=0.5' --referer 'https://www.python.org/downloads/release/python-363/' --header 'Upgrade-Insecure-Requests: 1' 'https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tar.xz' --output 'Python-3.6.3.tar.xz'
tar xfv Python-3.6.3.tar.xz
trsh Python-3.6.3.tar.xz
cd Python-3.6.3
./configure
sudo make
sudo make install
cd ~/ #Python 3.* should now be fully installed.

#Ruby Install
#Installing RVM first:
#Install mpapis public key (might need `gpg2` and or `sudo`)
sudo apt-get update
sudo gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
# Download the installer
sudo sh curl -O https://raw.githubusercontent.com/rvm/rvm/master/binscripts/rvm-installer
sudo sh curl -O https://raw.githubusercontent.com/rvm/rvm/master/binscripts/rvm-installer.asc
# Verify the installer signature (might need `gpg2`), and if it validates run the installer:
sudo gpg --verify rvm-installer.asc && sudo bash rvm-installer stable
#Now to install ruby:
sudo rvm install Ruby
